AddCSLuaFile()

SWEP.Base = "weapon_base"

SWEP.DrawCrosshair = false
SWEP.ViewModelFOV = 82
SWEP.ViewModelFlip = true

SWEP.Category = "FW Weapons"
SWEP.PrintName = ""
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Primary.Sound = Sound("Weapon_Pistol.Empty")
SWEP.Primary.Recoil = 1.5
SWEP.Primary.Damage = 10
SWEP.Primary.NumShots = 1
SWEP.Primary.Cone = 0.02
SWEP.Primary.Delay = 1

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "none"
SWEP.Primary.ClipMax = -1

SWEP.Secondary.ClipSize = 1
SWEP.Secondary.DefaultClip = 1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.ClipMax = -1

SWEP.WorldModel = Model("models/props_junk/watermelon01.mdl")

function SWEP:Initialize()
	self:SetWeaponHoldType("grenade")
end

function SWEP:ModifyWModelPos(pos, ang)
	self:SetModelScale(0.5, 0)

	pos = pos + ang:Right() * -3.5

	ang:RotateAroundAxis(ang:Forward(), -83)

	return pos, ang
end

function SWEP:ModifyProjectile(proj)
end

function SWEP:PrimaryAttack()
	self.Owner:SetAnimation( PLAYER_ATTACK1 )

	if SERVER then

		self.Owner.ProjectileThrowCount = (self.Owner.ProjectileThrowCount or 0) + 1
		hook.Call("FWProjectilePreThrow", GAMEMODE, self.Owner, self)

		local proj = ents.Create("projectile_base")
		proj.Model = self.WorldModel
		proj:SetPos(self.Owner:GetShootPos())
		proj.VelocityVector = self.Owner:GetAimVector()
		proj:SetOwner(self.Owner)
		proj.PVPDamage = self.Primary.Damage
		self:ModifyProjectile(proj)

		hook.Call("FWModifyProjectile", GAMEMODE, self, proj)

		proj:Spawn()
		
	end
	
	local delay = self.Primary.Delay * gmbp.CallCumulativeHook("AttackSpeed", 1, self.Owner) * (1/GetConVar("fw_quakefactor"):GetFloat())
	self:SetNextPrimaryFire(CurTime() + delay)

	self:EmitSound("weapons/slam/throw.wav")

end
function SWEP:SecondaryAttack()
end

function SWEP:DrawWorldModel()
	if not IsValid(self.Owner) --[[or self.Owner == LocalPlayer()]] then
		return self:DrawModel()
	end

	local att = self.Owner:GetAttachment(self.Owner:LookupAttachment("anim_attachment_RH"))
	local pos, ang = att.Pos, att.Ang
	if not pos or not ang then return end

	pos, ang = self:ModifyWModelPos(pos, ang)

	self:SetRenderOrigin(pos)
	self:SetRenderAngles(ang)
	self:SetupBones()
	self:DrawModel()
end