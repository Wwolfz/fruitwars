AddCSLuaFile()

SWEP.Base = "weapon_basic_base"

SWEP.Primary.Delay = 0.95
SWEP.Primary.Damage = 20

SWEP.WorldModel = Model("models/props/cs_italy/orange.mdl")

function SWEP:ModifyProjectile(proj)
	proj:SetTrailColor(Vector(230/255, 126/255, 34/255))
end

function SWEP:ModifyWModelPos(pos, ang)
	self:SetModelScale(1, 0)

	pos = pos + ang:Right() * -2.3

	return pos, ang
end