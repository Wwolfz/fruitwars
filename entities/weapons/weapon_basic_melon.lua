AddCSLuaFile()

SWEP.Base = "weapon_basic_base"

SWEP.Primary.Delay = 1.2

SWEP.Primary.Damage = 25

SWEP.WorldModel = Model("models/props_junk/watermelon01.mdl")

function SWEP:ModifyProjectile(proj)
	proj:SetTrailColor(Vector(46/255, 204/255, 113/255))
end

function SWEP:ModifyWModelPos(pos, ang)
	self:SetModelScale(0.5, 0)

	pos = pos + ang:Right() * -3.5

	ang:RotateAroundAxis(ang:Forward(), -83)

	return pos, ang
end