AddCSLuaFile()

SWEP.Base = "weapon_basic_base"

SWEP.Primary.Delay = 0.7

SWEP.Primary.Damage = 15

SWEP.WorldModel = Model("models/props/cs_italy/bananna.mdl")

function SWEP:ModifyProjectile(proj)
	proj:SetTrailColor(Vector(241/255, 196/255, 15/255))
end

function SWEP:ModifyWModelPos(pos, ang)
	self:SetModelScale(1, 0)

	pos = pos + ang:Right() * -0.5

	ang:RotateAroundAxis(ang:Forward(), -83)

	return pos, ang
end