
function EFFECT:Init(data)
   self:SetPos(data:GetOrigin())
   self.StartTime = CurTime()
   self.Length = 0.5
end

function EFFECT:GetFraction()
   return (CurTime()-self.StartTime) / self.Length
end

function EFFECT:Think()
   return self:GetFraction() < 1
end

local sin,cos,rad = math.sin,math.cos,math.rad; --Only needed when you constantly calculate a new polygon, it slightly increases the speed.
local function GenerateFancyPoly(x,y,radius,quality)
    local circle = {};
    local tmp = 0;
    for i=1,quality do
      local radius = radius
        tmp = rad(i*360)/quality
        circle[i] = {x = x + cos(tmp)*radius,y = y + sin(tmp)*radius};
    end
    return circle;
end

local matOutline = Material("phoenix_storms/wire/pcb_green")
function EFFECT:Render()
   cam.Start3D2D(self:GetPos(), Angle(0, 0, 0), 1)
   draw.NoTexture()

   render.ClearStencil()
   render.SetStencilEnable( true )

   local function HollowCircle(inner_radius, outer_radius)
      render.SetStencilFailOperation( STENCILOPERATION_KEEP )
      render.SetStencilZFailOperation( STENCILOPERATION_REPLACE )
      render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
      render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )
      render.SetStencilReferenceValue( 1 )

      surface.SetDrawColor(0, 0, 0, 1)
      surface.DrawPoly(GenerateFancyPoly(0, 0, outer_radius, 32))

      render.SetStencilReferenceValue( 2 )

      surface.SetDrawColor(0, 0, 0, 1)
      surface.DrawPoly(GenerateFancyPoly(0, 0, inner_radius, 32))
   end
 
   local min = self:GetFraction() * 500
   HollowCircle(min, min+50)

   render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
   render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
   render.SetStencilReferenceValue( 1 )
 
   render.SetColorModulation(0, 1, 0)
   render.SuppressEngineLighting(true)
   render.SetMaterial( matOutline )

   render.DrawScreenQuad()
 
   render.SuppressEngineLighting(false)
   render.SetColorModulation(1, 1, 1)

   render.SetStencilEnable( false )

   cam.End3D2D()
end

