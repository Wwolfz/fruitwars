function EFFECT:Init(data)
  self.StartTime = CurTime()
  self.Length = 2.5
  
  self:SetPos(data:GetOrigin())
  self.Vel = VectorRand()*0.5
  self.Vel.z = 1

  self.Damage = data:GetRadius()
  self.DamageType = data:GetScale()
end

function EFFECT:Think()
  if (self.StartTime + self.Length) < CurTime() then
    return false
  end
  self.Vel = self.Vel + Vector(0, 0, -0.015) -- gravity
  self:SetPos(self:GetPos() + self.Vel)
  return true
end

surface.CreateFont("FWHitIndFont", {
  font = "Roboto",
  size = 64,
  weight = 800
})

function EFFECT:Render()
  local frac = (CurTime() - self.StartTime) / self.Length
  
  local myangle = EyeAngles()
  myangle.r = 0
  myangle.p = 0
  
  myangle:RotateAroundAxis(myangle:Right(), 90)
  myangle:RotateAroundAxis(myangle:Up(), -90)

  local r, g, b = 255, 255, 255
  if self.DamageType == DMG_ACID then -- it's poison really
    r, g, b = 102, 204, 26
  end

  --local scale = math.Clamp(LocalPlayer():GetPos():Distance(self:GetPos())*1000, 0.1, 1)

  cam.Start3D2D(self:GetPos(), myangle, 0.5)
    draw.SimpleTextOutlined(tostring(math.Round(self.Damage)), "FWHitIndFont", 0, 0, Color(255, 0, 0, (1-frac)*255), _, _, 3, Color(r, g, b, (1-frac)*255))
  cam.End3D2D()
end