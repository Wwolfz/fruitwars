
AddCSLuaFile()

ENT.Type = "anim"
ENT.Model = Model("models/props/cs_office/radio.mdl")
ENT.IsFWProjectile = true

ENT.PVPDamage = 10

ENT.TargetSpeed = 10000

function ENT:SetupDataTables()
	self:NetworkVar("Vector", 0, "TrailColor")
	self:NetworkVar("Vector", 1, "HomingTarget")
	if SERVER then self:SetTrailColor(Vector(1, 1, 1)) end
end

function ENT:Initialize()
	if SERVER then
		self:SetModel(self.Model)

		self:PhysicsInitBox(Vector(-10, -10, -10), Vector(10, 10, 10))
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_BBOX)

		local phys = self:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableGravity( false )
			phys:Wake()
		end

		self:StartMotionController()
		self:SetTrigger(true)

		self.NormalCollisionBounds = {self:GetCollisionBounds()}
		if self.QueueScale then
			self:SetFWModelScale(self.QueueScale)
		else
			self:SetFWModelScale(8, true) -- Makes the hitbox a bit larger and thus easier to hti people with
		end

		self:PrecacheGibs()
	end
	--self:SetCollisionGroup(COLLISION_GROUP_NONE)
end

function ENT:SetFWModelScale(scale, no_visual)
	if not self.NormalCollisionBounds then
		self.QueueScale = scale
		return
	end
	if not no_visual then self:SetModelScale(scale, 0) end

	local cbscale = scale*0.5 -- Because we incerase both sides, this needs to be half the model scale
	self:SetCollisionBounds(self.NormalCollisionBounds[1]*cbscale, self.NormalCollisionBounds[2]*cbscale)

	self.FWColScale = scale
end

function ENT:Think()
	if CLIENT and gmbp.TickEvery(self, "ParticleTrail", 0.02) then
		local tc = self:GetTrailColor()
		if not (tc.x == 0 and tc.y == 0 and tc.z == 0) then
			gmbp.SpawnParticle(self:GetPos(), {
				DieTime = 0.4,
				EndSize = 0,
				Color = Color(tc.x*255, tc.y*255, tc.z*255)
			})
		end
		local min, max = self:GetCollisionBounds()
		 debugoverlay.BoxAngles(self:GetPos(), min, max, self:GetAngles(), 0.1, Color(255, 255, 255, 10))
	end
end

local an = function(start, targ, incr)
	if targ>start then
		return math.min(start+incr, targ)
	end
	return math.max(start-incr, targ)
end

local function ApproachVectorObj(cur, targ, incr, zfix)
	return Vector(an(cur.x, targ.x, incr), an(cur.y, targ.y, incr), an(cur.z, targ.z, incr*(zfix and 0.2 or 1)))
end

function ENT:GetHomingTargetVector()
	local targ = self:GetHomingTarget()
	if not (targ.x == 0 and targ.y == 0 and targ.z == 0) then
		return targ, 0.05
	end
	if self.HomingPreset then
		local hp = self.HomingPreset
		local targv
		if hp.type == "nearenemy" then
			for _,ply in pairs(player.GetAll()) do
				if ply ~= self:GetOwner() and ply:Alive() and self:GetPos():Distance(ply:GetPos()) < 1500 then
					targv = ply:LocalToWorld(ply:OBBCenter())
				end
			end
		end
		return targv, hp.strength
	end
end

function ENT:PhysicsSimulate( phys, deltatime )

	phys:Wake()

	local vel = (self.VelocityVector or Vector(0, 0, 0)) * self.TargetSpeed

	local targ, strength = self:GetHomingTargetVector()
	if targ then
		local targangle = (targ - self:GetPos()):GetNormalized()
		local curangle = vel:GetNormalized()

		local newangle = ApproachVectorObj(curangle, targangle, strength or 0.05, true)

		--[[ Homing debug, uncomment if you need to know exactly how homing works (use host_timescale 0.1 or sumthing as well)

		if gmbp.TickEvery(self, "HomingDebug", 0.1) then
			local pos = self:GetPos()
			debugoverlay.Line(pos, pos+curangle*100, 2, Color(255, 0, 0))
			debugoverlay.Line(pos, pos+targangle*100, 2, Color(0, 255, 0))
			debugoverlay.Line(pos, pos+newangle*100, 2, Color(255, 255, 0))
		end]]

		self.VelocityVector = newangle:GetNormalized()
	end

	local physVel = phys:GetVelocity()
	if physVel:Length() >= self.TargetSpeed then vel = vector_origin end

	return Vector(0, 0, 0), vel, SIM_GLOBAL_ACCELERATION
	
end

function ENT:DmgRadius(inflictor, attacker, origin, radius, damage, ...)

	local ignoreTable = {...}

	local info = DamageInfo( )
		info:SetDamageType( DMG_GENERIC )
		info:SetDamagePosition( origin )
		info:SetMaxDamage( damage )
		info:SetDamage( damage )
		info:SetAttacker( attacker )
		info:SetInflictor( inflictor )

	for k, v in ipairs( ents.FindInSphere(origin, radius) ) do
		if IsValid( v ) and v:Health( ) > 0 and not table.HasValue(ignoreTable, v) then
			local p = v:GetPos( )
			 
			info:SetDamage( damage * ( 1 - p:Distance( origin ) / radius ) )
			info:SetDamageForce( ( p - origin ):GetNormalized( ) * 512 )
			v:TakeDamageInfo( info )
		end
	end
end

function ENT:Touch(ent)

	if ent == self:GetOwner() then return end

	local ent = ent

	self:EmitSound("npc/fast_zombie/foot4.wav")

	local ptbl = {
		Velocity = Vector(0, 0, 0) * 25,
		DieTime = 1,
		StartSize = 150,
		StartAlpha = 200,
		EndAlpha = 0,
		DynLight = true,
		Color = Color(self:GetTrailColor().x*255, self:GetTrailColor().y*255, self:GetTrailColor().z*255)
	}
	gmbp.SpawnParticle(self:GetPos(), ptbl)

	if ent:IsPlayer() then
		ptbl.Color = Color(255, 0, 0)
		local dmginfo = DamageInfo()
		dmginfo:SetDamage(self.PVPDamage)
		dmginfo:SetAttacker(self:GetOwner())
		dmginfo:SetInflictor(self)

		if self.ModifyDmgInfo then self:ModifyDmgInfo(ent, dmginfo) end

		ent:TakeDamageInfo(dmginfo)

		if self.AfterTargDamaged then self:AfterTargDamaged(ent) end
	end

	if self.BlastDamage and self.BlastRadius then
		self:DmgRadius(self, self:GetOwner(), ent:GetPos(), self.BlastRange, self.BlastDamage, self:GetOwner(), unpack(team.GetPlayers(self:GetOwner():Team())))
	end

	self:GibBreakClient(Vector(0, 0, 0)) -- data.HitNormal
	self:Remove()
end
function ENT:PhysicsCollide( data, physobj )
	if self.FWCollideOverride and self:FWCollideOverride(data, physobj) then
		return
	end
	self:EmitSound("npc/fast_zombie/foot4.wav")


	local ptbl = {
		Velocity = data.HitNormal * 25,
		DieTime = 1,
		StartSize = 150,
		StartAlpha = 200,
		EndAlpha = 0,
		DynLight = true,
		Color = Color(self:GetTrailColor().x*255, self:GetTrailColor().y*255, self:GetTrailColor().z*255)
	}
	gmbp.SpawnParticle(self:GetPos(), ptbl)


	if data.HitEntity:IsPlayer() then
		ptbl.Color = Color(255, 0, 0)
		local dmginfo = DamageInfo()
		dmginfo:SetDamage(self.PVPDamage)
		dmginfo:SetAttacker(self:GetOwner())
		dmginfo:SetInflictor(self)

		if self.ModifyDmgInfo then self:ModifyDmgInfo(data.HitEntity, dmginfo) end

		data.HitEntity:TakeDamageInfo(dmginfo)

		if self.AfterTargDamaged then self:AfterTargDamaged(data.HitEntity) end
	end

	if self.BlastDamage and self.BlastRadius then
		self:DmgRadius(self, self:GetOwner(), data.HitPos, self.BlastRange, self.BlastDamage, self:GetOwner(), unpack(team.GetPlayers(self:GetOwner():Team())))
	end

	self:GibBreakClient(data.HitNormal)
	self:Remove()
end

hook.Add("EntityTakeDamage", "DisableProjPhysDamage", function(ent, dmginfo)
	if dmginfo:GetDamageType() == DMG_CRUSH and dmginfo:GetAttacker().IsFWProjectile then
		dmginfo:ScaleDamage(0)
		dmginfo:SetDamageForce(Vector(0, 0, 0))
	end
end)

function ENT:Draw()
	local start_scale = self:GetModelScale()
	render.MaterialOverride(Material("models/props_combine/stasisshield_sheet"))
	self:SetModelScale(start_scale+1, 0)
	self:DrawModel()
	render.MaterialOverride()
	self:SetModelScale(start_scale, 0)
	self:DrawModel()
end
