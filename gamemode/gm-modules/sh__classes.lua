CLASS_MELON = 1
CLASS_ORANGE = 2
CLASS_BANANA = 3

local plymeta = FindMetaTable("Player")
function plymeta:GetFWClass()
	--[[local cls = player_manager.GetPlayerClass(self)
	if not cls then return 0 end
	local spec = string.match(cls, "player_(%w+)")
	if spec == "melon" then return CLASS_MELON
	elseif spec == "orange" then return CLASS_ORANGE
	elseif spec == "banana" then return CLASS_BANANA end
	return 0]]
	return self:GetNWInt("fwcls", 0)
end

if SERVER then
	function plymeta:SetFWClass(cls)
		self:SetNWInt("fwcls", cls)

		local pclspost
		if cls == CLASS_MELON then pclspost = "melon"
		elseif cls == CLASS_ORANGE then pclspost = "orange"
		elseif cls == CLASS_BANANA then pclspost = "banana" end

		if pclspost then 
			player_manager.SetPlayerClass(self, "player_" .. pclspost)
		end
	end

	function plymeta:AddHealth(num)
		self:SetHealth(math.min(self:Health() + num, self:GetMaxHealth()))
	end
end

function plymeta:IsActivePlayer()
	return self:Team() == TEAM_ONE or self:Team() == TEAM_TWO
end

function plymeta:GetFWCooldown(idx)
	local vec = self:GetFWCooldowns()
	if idx == 1 then return vec.x
	elseif idx == 2 then return vec.y
	elseif idx == 3 then return vec.z end
	return CurTime()+1
end
function plymeta:SetFWCooldown(idx, val)
	local vec = self:GetFWCooldowns()
	if idx == 1 then self:SetFWCooldowns(Vector(val, vec.y, vec.z))
	elseif idx == 2 then self:SetFWCooldowns(Vector(vec.x, val, vec.z))
	elseif idx == 3 then self:SetFWCooldowns(Vector(vec.x, vec.y, val)) end
end

if SERVER then
	function plymeta:SetMovementSpeedMul(mul)
		mul = mul or 1
		local defspeed = self.FW_DefMovementSpeed or 300

		if self:GetWalkSpeed() == defspeed*mul then return end

		self:SetWalkSpeed(defspeed * mul)
		self:SetRunSpeed(defspeed * mul)
	end
end