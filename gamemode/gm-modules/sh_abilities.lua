local attack_builder_meta = {
	-- Private methods
	AddChain = function(self, type, we)
		we.type = type
		table.insert(self.Chain, we)
	end,
	PrependChain = function(self, type, we)
		we.type = type
		table.insert(self.Chain, 1, we)
	end,
	Execute = function(self, player)
		local first = table.remove(self.Chain, 1)
		if not first then return end

		--MsgN("Executing ", first.type)

		self.Player = self.Player or player
		if not IsValid(self.Player) then
			return MsgN("Terminated attack builder exec because player invalid")
		end

		self.HookIndex = self.HookIndex or (self.Player:EntIndex().."x"..CurTime())

		if first.type == "wait" then
			timer.Simple(first.length, function() self:Execute() end)
			return
		elseif first.type == "condwait" then -- Note: condwait continues if returned true and terminates whole queue if returned false. Return nil for neither
			local timerid = "attack_builder_condexec" .. CurTime()
			timer.Create(timerid, first.delay or 0.2, 0, function()
				local function destroy() timer.Destroy(timerid) end
				if not IsValid(self.Player) then return destroy() end
				local condcall = first.cond(self)
				if condcall == false then return destroy() end
				if condcall ~= true then return end

				destroy()
				self:Execute(player)
			end)
			return
		elseif first.type == "dox" then
			timer.Create("attack_builder_exec" .. CurTime(), first.delay, first.times, first.fn, self)
		elseif first.type == "do" then
			first.fn(self)
			if first.terminate then return end
		end
		self:Execute(player)
	end,

	-- Base methods that are the basis for implementations below
	Wait = function(self, length)
		self:AddChain("wait", {length=length})
		return self
	end,
	DoXTimes = function(self, times, timebetween, fn)
		self:AddChain("dox", {times=times, delay=timebetween, fn=fn})
		return self
	end,
	Do = function(self, fn)
		self:AddChain("do", {fn=fn})
		return self
	end,

	-- Some commonly used implementations

	WaitUntilAttack = function(self, maxwait)
		self:AddChain("do", {
			fn = function(self)
				hook.Add("FWProjectilePreThrow", "WaitingUAttack" .. self.HookIndex, function(ply)
					if ply == self.Player then
						hook.Remove("FWProjectilePreThrow", "WaitingUAttack" .. self.HookIndex)
						return self:Execute()
					end
				end)
			end,
			terminate = true
		})
		return self
	end,
	-- Gives buff and waits until it's used/player doesnt have it anymore
	GiveBuffAndWait = function(self, buffnm, datatbl)
		local buffid
		self:AddChain("do", {fn=function(self)
			local buff = self.Player:GiveFWBuff(buffnm, datatbl)
			self.Buffs = self.Buffs or {}
			self.Buffs[buff.name] = buff

			buffid = buff.id
		end})
		self:AddChain("condwait", {
			cond = function(self)
				if not self.Player:HasFWBuff(buffid) then return true end
			end
		})
		return self
	end,
	GiveBuff = function(self, buffnm, datatbl)
		self:AddChain("do", {fn=function(self)
			local buff = self.Player:GiveFWBuff(buffnm, datatbl)
			self.Buffs = self.Buffs or {}
			self.Buffs[buff.name] = buff
		end})
		return self
	end,
	StopBuff = function(self, buffnm)
		self:AddChain("do", {fn=function(self)
			if self.Buffs and self.Buffs[buffnm] then
				self.Buffs[buffnm]:Stop()
			end
		end})
		return self
	end,
	AddHook = function(self, id, fn)
		self:AddChain("do", {fn=function(self)
			hook.Add(id, "AbilityHook" .. self.HookIndex .. id, fn)
		end})
		return self
	end,
	RemoveHook = function(self, id)
		self:AddChain("do", {fn=function(self)
			hook.Remove(id, "AbilityHook" .. self.HookIndex .. id)
		end})
		return self
	end,
	AddCHook = function(self, id, fn)
		self:AddChain("do", {fn=function(self)
			gmbp.AddCumulativeHook(id, "AbilityHook" .. self.HookIndex .. id, fn)
		end})
		return self
	end,
	RemoveCHook = function(self, id)
		self:AddChain("do", {fn=function(self)
			gmbp.RemoveCumulativeHook(id, "AbilityHook" .. self.HookIndex .. id)
		end})
		return self
	end,
}
attack_builder_meta.__index = attack_builder_meta

gmbp.BuildAttack = function()
	local tbl = {Chain = {}}
	setmetatable(tbl, attack_builder_meta)
	return tbl
end

gmbp.Abilities = {
	[CLASS_MELON] = {
		[1] = {
			name = "Melon Smash",
			description = "Calls Melon Force One to strike the target area",
			icon = gmbp.AbilityIcons.melon_strike,

			select_type = "aoe",
			cooldown = 20,
			on_select = function(ply, pos)
				return gmbp.BuildAttack()
						:Do(function()
							sound.Play("npc/env_headcrabcanister/launch.wav", pos, 75, 90, 1)
						end)
						:DoXTimes(5, 0.18, function()
							local fr = 6
							for i=1,fr do
								local rad = (math.pi*2) / fr * i
								gmbp.SpawnParticle(pos + Vector(math.cos(rad)*100, math.sin(rad)*100, 0), {
									DieTime = 0.4,
									Velocity = Vector(0, 0, 100)
								})
							end
						end)
						:Wait(1)
						:DoXTimes(6, 0.2, function()
							local tpos = pos + Vector((math.random()-0.5)*600, (math.random()-0.5)*600, 0)
							local skytrace = util.QuickTrace(tpos, Vector(0, 0, 1000))
							tpos.z = skytrace.HitPos.z - 50

							local proj = ents.Create("projectile_base")
							proj.Model = Model("models/props_junk/watermelon01.mdl")
							proj:SetPos(tpos)
							proj.VelocityVector = Vector(0, 0, -10000)
							proj:SetOwner(ply)

							proj.BlastRange = 300
							proj.BlastDamage = 30

							proj.FWCollideOverride = function(self, data, physobj)
								self:EmitSound("npc/env_headcrabcanister/explosion.wav")
								for i=1,5 do
									gmbp.SpawnParticle(self:GetPos() + VectorRand()*30, {
										Velocity = VectorRand() * 100,
										StartSize = math.random() * 300,
										DieTime = 0.5 + math.random() * 0.5,
										Color = Color(math.random()*255, math.random()*255, math.random()*255)
									})
								end
							end
							proj:Spawn()
							proj:EmitSound("npc/env_headcrabcanister/incoming.wav")
						end)
			end
		},
		[2] = {
			name = "Fruit Punch",
			description = "The next basic attack will stun for a short time",
			icon = gmbp.AbilityIcons.melon_stun,

			select_type = "trigger",
			cooldown = 3,
			on_select = function(ply, pos)
				return gmbp.BuildAttack()
						:GiveBuff("abilitystandby", {length=5})
						:WaitUntilAttack(5)
						:GiveBuff("stunprojectile", {length=5, modlength=1.4})
						:StopBuff("abilitystandby")
						:Do(function()
							ply:EmitSound("physics/body/body_medium_impact_soft2.wav")
						end)
			end
		},
		[3] = {
			name = "The Juicer",
			description = "The next basic attack will not be so basic",
			 icon = gmbp.AbilityIcons.melon_big,

			select_type = "trigger",
			cooldown = 2,
			on_select = function(ply, pos)
				return gmbp.BuildAttack()
						:GiveBuff("abilitystandby", {length=5})
						:WaitUntilAttack(5)
						:GiveBuff("resizeprojectile", {mul=3.5, dmgmul = 2})
						:StopBuff("abilitystandby")
						:Do(function()
							ply:EmitSound("physics/body/body_medium_impact_soft2.wav")
						end)
			end
		},
	},
	[CLASS_ORANGE] = {
		[1] = {
			name = "Rotten Fruit",
			description = "The next basic attack will do poison damage.",
			icon = gmbp.AbilityIcons.orange_poison,

			select_type = "trigger",
			cooldown = 2,
			on_select = function(ply, pos)
				return gmbp.BuildAttack()
						:GiveBuff("abilitystandby", {length=5})
						:WaitUntilAttack(5)
						:GiveBuff("hpmodprojectile", {length=5, off=-5, modlength=6, dmgtype= DMG_ACID })
						:StopBuff("abilitystandby")
						:Do(function()
							ply:EmitSound("physics/body/body_medium_impact_soft2.wav")
						end)
			end

		},
		[2] = {
			name = "And juice for all",
			description = "Gives allies in radius a speed and a health boost",
			icon = gmbp.AbilityIcons.orange_boost,

			select_type = "trigger",
			cooldown = 15,
			on_select = function(ply, pos)
				return gmbp.BuildAttack()
						:Do(function()
							ply:EmitSound("items/suitchargeok1.wav")
						end)
						:Wait(0.35)
						:Do(function()
							gmbp.ToPlayersInRadius(ply:GetPos(), 512, function(ply, dist, frac)
								ply:AddHealth(frac*12)
								ply:GiveFWBuff("movespeed", {length=frac*2, mul=1+frac*0.15})
							end)
							for i=0,2 do
								gmbp.SpawnParticle(ply:EyePos(), {
									DieTime = 2,
									EndSize = 0,
									Velocity = Vector((math.random()-0.5)*90, (math.random()-0.5)*90, 60),
									Gravity = Vector(0, 0, -100),
									Color = Color(0, 255, 0)
								})
							end
							local eff = EffectData()
							eff:SetOrigin(ply:GetPos())
							util.Effect("orange_healthwave", eff, true, true)
						end)
			end
		},
		[3] = {
			name = "The Sapper",
			description = "Trades some of your health for an instant extra basic attack",
			icon = gmbp.AbilityIcons.orange_extra,

			select_type = "trigger",
			cooldown = 1,
			on_select = function(ply, pos)
				return gmbp.BuildAttack()
						:Do(function()
							ply:TakeDamage(5)

							local proj = ents.Create("projectile_base")
							proj.Model = ply:GetActiveWeapon().WorldModel
							proj:SetPos(ply:GetShootPos())
							proj.VelocityVector = ply:GetAimVector() * 8000
							proj:SetOwner(ply)
							proj.PVPDamage = IsValid(ply:GetActiveWeapon()) and ply:GetActiveWeapon().Primary.Damage or 1
							proj:Spawn()
						end)
			end
		}
	},
	[CLASS_BANANA] = {
		[1] = {
			name = "Leap of Fruaith",
			description = "Leaps forward and giving small move and attack speed boosts",
			icon = gmbp.AbilityIcons.banana_charge,

			select_type = "trigger",
			cooldown = 8,
			on_select = function(ply, pos)
				return gmbp.BuildAttack()
						:Do(function()
							ply:EmitSound("npc/antlion_guard/angry1.wav")
							ply:SetVelocity((ply:GetUp() * 250) + (ply:GetForward() * 400))
						end)
						:GiveBuff("movespeed", {length=1, mul=1.2})
						:GiveBuff("attackspeed", {length=1, mul=0.8})
			end
		},
		[2] = {
			name = "The Surplus Attack",
			description = "Makes the following three basic attacks rapid and home in on enemies",
			icon = gmbp.AbilityIcons.banana_spam,

			select_type = "trigger",
			cooldown = 13,
			on_select = function(ply, pos)
				local proj
				return gmbp.BuildAttack()
						:Do(function()
							ply:EmitSound("npc/combine_gunship/attack_stop2.wav")
						end)
						:GiveBuff("abilitystandby", {length=5})
						:WaitUntilAttack(5)
						:GiveBuff("attackspeed", {length=0.4, mul=0.24})
						:GiveBuff("homingprojectiles", {projectiles=3, strength=0.15})
						:StopBuff("abilitystandby")
						:Do(function()
							ply:EmitSound("npc/antlion/digdown1.wav")
						end)
			end
		},
		[3] = {
			name = "Delivery Man",
			description = "Gives a big speed boost for a short time",
			icon = gmbp.AbilityIcons.banana_sprint,

			select_type = "trigger",
			cooldown = 15,
			on_select = function(ply, pos)
				return gmbp.BuildAttack()
						:GiveBuff("movespeed", {length = 4, mul = 1.5})
			end
		}
	}
}