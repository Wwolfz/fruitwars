-- Can be used to create a simple timer. Returns true if should run
-- Ex. usage:
--
-- function SWEP:Think()
-- 	   if gmbp.TickEvery(self, "PlaySound", 1) then self:PlaySound() end
-- end
function gmbp.TickEvery(self, id, delay)
	if not self["Tick_" .. id] or self["Tick_" .. id] < CurTime() then
		self["Tick_" .. id] = CurTime() + delay
		return true
	end
	return false
end

function gmbp.ToPlayersInRadius(origin, radius, fn)
	for k, v in ipairs( ents.FindInSphere(origin, radius) ) do
		if v:IsPlayer() and v:Alive() then
			local dist = v:GetPos():Distance(origin)
			local fraction = (1 - dist/radius) 
			fn(v, dist, fraction)
		end
	end
end

gmbp.CumulativeHooks = gmbp.CumulativeHooks or {}

function gmbp.AddCumulativeHook(name, id, fn)
	gmbp.CumulativeHooks[name] = gmbp.CumulativeHooks[name] or {}
	gmbp.CumulativeHooks[name][id] = fn
end

function gmbp.RemoveCumulativeHook(name, id)
	if gmbp.CumulativeHooks[name] then
		gmbp.CumulativeHooks[name][id] = nil
	end
end

function gmbp.CallCumulativeHook(name, def, ...)
	def = def or 1
	
	local ch = gmbp.CumulativeHooks[name]
	if ch then
		for _,hook in pairs(ch) do
			def = def * (hook(...) or 1)
		end
	end

	return def
end

