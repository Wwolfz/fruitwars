ROUND_PRE = 1
ROUND_ACTIVE = 2
ROUND_POST = 3

gmbp.RoundStateNames = {
	[ROUND_PRE] = "Preparing",
	[ROUND_ACTIVE] = "Active",
	[ROUND_POST] = "End"
}

function gmbp.GetRoundState()
	return GetGlobalInt("roundstate", ROUND_PRE) or ROUND_PRE
end