
CreateConVar("fw_debugmode", "0")

local fw_quakefactor = CreateConVar("fw_quakefactor", "1", FCVAR_ARCHIVE)

function GM:PlayerSpawn(ply)
	if ply:GetFWClass() == 0 or not ply:IsActivePlayer() then
		GAMEMODE:PlayerSpawnAsSpectator(ply)
		return
	end
	ply:UnSpectate()

	player_manager.OnPlayerSpawn( ply )
	player_manager.RunClass( ply, "Spawn" )

	local ors = ply:GetRunSpeed()
	ply:SetRunSpeed(ors * fw_quakefactor:GetFloat())
	ply:SetWalkSpeed(ors * fw_quakefactor:GetFloat())

	ply.FW_DefMovementSpeed = ply:GetRunSpeed()

	hook.Call("PlayerLoadout", GAMEMODE, ply)
	hook.Call("PlayerSetModel", GAMEMODE, ply)

	--ply:SetModel("models/player/alyx.mdl")

	local clr = team.GetColor(ply:Team())
	ply:SetPlayerColor(Vector(clr.r/255, clr.g/255, clr.b/255))
end

function GM:PlayerInitialSpawn(ply)
	ply:SetTeam(team.BestAutoJoinTeam())
	ply:SetFWClass(CLASS_ORANGE)
end

function GM:ScalePlayerDamage(ply, hitgroup, dmginfo)
	local attacker = dmginfo:GetAttacker()
	if attacker:IsPlayer() and attacker:Team() == ply:Team() then
		dmginfo:ScaleDamage(0)
	end
end

function GM:PlayerDeathThink( pl )
	if gmbp.GetRoundState() == ROUND_PRE then
		if pl:KeyPressed( IN_ATTACK ) or pl:KeyPressed( IN_ATTACK2 ) or pl:KeyPressed( IN_JUMP ) then
			pl:Spawn()
		end
	end
end

function GM:PlayerDeathSound()
	return true
end

util.AddNetworkString("fw_hitindicator")
function GM:EntityTakeDamage(target, dmginfo)
	if not dmginfo:GetAttacker():IsPlayer() then return end

	local eff = EffectData()
	eff:SetOrigin(target:EyePos())
	eff:SetRadius(dmginfo:GetDamage())
	eff:SetScale(dmginfo:GetDamageType())

	local filter = RecipientFilter()
	filter:AddPlayer(dmginfo:GetAttacker())
	util.Effect("hit_indicator", eff, true, filter)
end