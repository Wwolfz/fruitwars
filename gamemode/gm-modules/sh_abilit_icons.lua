-- This file contains functions to draw icons for abilities using gmod surface functions etc.
-- They are in separate file for clarity

-- This file is shared for load order reasons (must be loaded before sh_abilities)

gmbp.AbilityIcons = {}

local mat_cache = {}

local function query_mat_cache(mat)
	local mc = mat_cache[mat]
	if mc then return mc end

	mat_cache[mat] = Material(mat)
	return mat_cache[mat]
end

-- Good ones found from sewers of CSS etc:
--[[

sprites/spectator_3rdcam camera pointing right
vgui/hud/autoaim circle thing
vgui/hud/icon_check check icon
vgui/hud/icon_commentary abstract speech bubble with three dots
vgui/hud/icon_locked lock icon
vgui/cursors/hand hand icon
vgui/progressbar whiteyellow

NOTE: vgui/achievements/ excellent resource in pretty much every game

]]

local function GenerateIconDrawerFunc(mat)
	return function(w, h)
		if SERVER then return end
		local mat = query_mat_cache(mat)

	    surface.SetMaterial(mat)
	    surface.SetDrawColor(255, 255, 255)
	    surface.DrawTexturedRect(0, 0, w, h)
	end
end

local function CombineFuncs(f, s)
	return function(...) f(...) s(...) end
end

gmbp.AbilityIcons.melon_strike = CombineFuncs(GenerateIconDrawerFunc("gui/effects/ar2explosion.png"), GenerateIconDrawerFunc("vgui/zoom"))
gmbp.AbilityIcons.melon_stun = GenerateIconDrawerFunc("vgui/face/close_eyes")
gmbp.AbilityIcons.melon_big = GenerateIconDrawerFunc("vgui/hsv")

gmbp.AbilityIcons.orange_poison = GenerateIconDrawerFunc("vgui/achievements/hl2_beat_toxictunnel")
gmbp.AbilityIcons.orange_boost = GenerateIconDrawerFunc("vgui/ico_friend_indicator_alone")
gmbp.AbilityIcons.orange_extra = CombineFuncs(GenerateIconDrawerFunc("vgui/ico_box14"), GenerateIconDrawerFunc("vgui/zoom"))

gmbp.AbilityIcons.banana_charge = CombineFuncs(GenerateIconDrawerFunc("vgui/hand"), GenerateIconDrawerFunc("vgui/spawnmenu/hover"))
gmbp.AbilityIcons.banana_spam = GenerateIconDrawerFunc("vgui/loading-rotate")
gmbp.AbilityIcons.banana_sprint = GenerateIconDrawerFunc("trails/electric")