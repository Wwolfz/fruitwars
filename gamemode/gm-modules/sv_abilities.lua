util.AddNetworkString("fw_ability")

net.Receive("fw_ability", function(len, cl)
	local abilityid = net.ReadUInt(8)
	local pos = net.ReadVector()

	if cl:GetFWCooldown(abilityid) >= CurTime() then return end

	gmbp.Abilities[cl:GetFWClass()][abilityid].on_select(cl, pos):Execute(cl)

	local cooldown = cvars.Bool("fw_debugmode") and 1 or gmbp.Abilities[cl:GetFWClass()][abilityid].cooldown
	cl:SetFWCooldown(abilityid, CurTime() + cooldown)
end)

hook.Add("Think", "ModifyPlayerSpeed", function()
	for _,ply in pairs(player.GetAll()) do
		local mvspeed = gmbp.CallCumulativeHook("MovementSpeed", 1, ply)
		ply:SetMovementSpeedMul(mvspeed)
	end
end)