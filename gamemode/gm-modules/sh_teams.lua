TEAM_ONE = 1
TEAM_TWO = 2

team.SetUp(TEAM_ONE, "Starsky", HSVToColor(5, 0.6, 0.75))
team.SetUp(TEAM_TWO, "Watermania", HSVToColor(204, 0.6, 0.86))

local team_spawns = {
	[TEAM_ONE] = "info_player_terrorist",
	[TEAM_TWO] = "info_player_counterterrorist"
}

hook.Add("PlayerSelectSpawn", "SelectTeamSpawn", function(ply)
	local spawn_ent = team_spawns[ply:Team()] or "info_player*"
	return table.Random(ents.FindByClass(spawn_ent))
end)