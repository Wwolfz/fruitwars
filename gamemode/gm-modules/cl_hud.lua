
if IsValid(gmbp.AbilityBar) then gmbp.AbilityBar:Remove() end -- lua refresh fix

concommand.Add("fw_refreshabilitybar", function()
    if IsValid(gmbp.AbilityBar) then gmbp.AbilityBar:Remove() end
end)

-- Cba to make own tooltip implementation now, so let's set the gmod tooltip's delay to zero
RunConsoleCommand("tooltip_delay", "0")

surface.CreateFont("AbilityHotkey", {
    font = "Roboto",
    size = 20,
    weight = 800
})

local function CreateAbilityBar()
    if IsValid(gmbp.AbilityBar) then gmbp.AbilityBar:Remove() end

    gmbp.AbilityBarMFWC = LocalPlayer():GetFWClass()

    local abilitybar = vgui.Create("DPanel")
    gmbp.AbilityBar = abilitybar
    abilitybar:SetPaintBackground(false)
    abilitybar.PerformLayout = function(self)
        self:SetPos(ScrW()/2-200, ScrH()-140)
        self:SetSize(400, 140)

        -- Width of each ability button
        local abw = 100

        -- Margin between ability buttons
        local abgap = 10

        -- Start x for ability buttons = width/2 - total_ability_btns_width/2 - single_gap
        --      Not sure why single gap is there only once but works and ehh
        local abstartx = (self:GetWide()/2)-((abw)*(3/2))-abgap

        -- Add ability buttons
        for i=0,2 do
            local fwcls = LocalPlayer():GetFWClass()
            if fwcls == 0 then continue end
            local ability = gmbp.Abilities[fwcls][i+1]
            if not ability then continue end

            local p = self.AbilityButtons[i]
            p:SetPos(abstartx + (abw+abgap)*i, 35)
            p:SetSize(abw, 100)
            p:SetTooltip(ability.name .. "\n" .. ability.description)

            local mat = type(ability.icon) == "string" and Material(ability.icon)

            p.Paint = function(self, w, h)
                local frac = math.Clamp((LocalPlayer():GetFWCooldown(i+1) - CurTime()) / ability.cooldown, 0, 1)

                local bg_hsv = {ColorToHSV(team.GetColor(LocalPlayer():Team()))}
                local bg_color = self.Hovered and
                                                HSVToColor(bg_hsv[1], bg_hsv[2]+0.2, bg_hsv[3]) or
                                                HSVToColor(bg_hsv[1], bg_hsv[2]+0.1, bg_hsv[3])

                --draw.RoundedBox(4, 0, 0, w, h, bg_color)
                surface.SetDrawColor(bg_color)
                surface.DrawRect(0, 0, w, h)

                if mat then
                    surface.SetMaterial(mat)
                    surface.SetDrawColor(255, 255, 255)
                    surface.DrawTexturedRect(16, 16, 64, 64)
                else
                    ability.icon(w, h) -- icon is a function
                end

                if frac > 0.05 then draw.RoundedBox(4, 0, (1-frac)*h, w, h*frac, Color(0, 0, 0, 220)) end

                --draw.RoundedBoxEx(4, w-20, 0, 20, 20, Color(0, 0, 0, 250), false, true, false, false)
                surface.SetDrawColor(0, 0, 0, 250)
                surface.DrawRect(w-20, 0, 20, 20)

                draw.SimpleText(i+1, "AbilityHotkey", w-15, 0)
            end
        end
        local boffx, boffy = 0, 0
        for i,btn in pairs(abilitybar.BuffButtons) do
            btn:SetPos(boffx+(i-1)*32, boffy+0)
            btn:SetSize(32, 32)
        end
    end

    --[[local mat = Material( "pp/blurscreen" )
    abilitybar.Paint = function(self, w, h)
        surface.SetMaterial( mat )
        surface.SetDrawColor( 255, 255, 255 )
        mat:SetFloat( "$blur", 5.0 )
        mat:Recompute()
        if render then render.UpdateScreenEffectTexture() end
        surface.DrawTexturedRect( 0, 0, w, h )
    end]]

    abilitybar.AbilityButtons = {}
    for i=0,2 do
        local abilitybtn = vgui.Create("DButton", abilitybar)
        abilitybtn:SetText("")
        abilitybar.AbilityButtons[i] = abilitybtn
    end

    abilitybar.BuffIdButtons = {} -- BuffButtons table, but "id" given by server as the table key

    abilitybar.BuffButtons = {}
    local function AddBuffButton(name, id, length, data)
        local buffmt = gmbp.Buffs[name]

        local p = vgui.Create("DButton", abilitybar)
        abilitybar.BuffIdButtons[id] = p

        p:SetText("")
        p:SetTooltip(buffmt.description(data))

        p:SetColor(Color(255, 255, 255))

        local bufficon = Material(buffmt.icon)
        p.Paint = function(self, w, h)
            local cd = (self.CooldownFrac or 0)
            local alpha = (self.FadeOutFrac or 1) * 255

            local is_buff_good = buffmt.is_positive(data)

            local saturation = self.Hovered and 0.7 or 0.5
            local bg_clr
            if is_buff_good then
                bg_clr = HSVToColor(120+math.sin(CurTime())*10, saturation, 0.95)
            else
                bg_clr = HSVToColor(360+math.sin(CurTime())*10, saturation, 0.95)
            end
            bg_clr.a = alpha

            draw.RoundedBox(4, 0, 0, w, h, bg_clr)

            surface.SetDrawColor(255, 255, 255, alpha)
            surface.SetMaterial(bufficon)
            surface.DrawTexturedRect(6, 6, 20, 20)

            draw.RoundedBox(4, 0, 0, w, h*cd, Color(0, 0, 0, alpha*0.5))

        end

        if length ~= 0 then
            do -- Animation for timer
                local anim = p:NewAnimation(length, 0, nil)
                anim.Think = function(anim, panel, frac)
                    panel.CooldownFrac = frac
                end
            end
            do -- Animation for fadeout
                local anim = p:NewAnimation(0.5, length, nil, function()
                    p:Remove()
                    table.RemoveByValue(abilitybar.BuffButtons, p)

                    abilitybar:InvalidateLayout()
                end)

                anim.Think = function(anim, panel, frac)
                    panel.FadeOutFrac = 1-frac
                end
            end
        end

        table.insert(abilitybar.BuffButtons, p)
    end

    net.Receive("fw_plybuffed", function()
        local ply = net.ReadEntity()
        local buffname = net.ReadString()
        local buffid = net.ReadString()
        local bufflength = net.ReadFloat()
        local datatbl = net.ReadTable()

        AddBuffButton(buffname, buffid, bufflength, datatbl)
    end)
    net.Receive("fw_plybuffaborted", function()
        local ply = net.ReadEntity()
        local buffid = net.ReadString()

        if IsValid(abilitybar.BuffIdButtons[buffid]) then
            table.RemoveByValue(abilitybar.BuffButtons, abilitybar.BuffIdButtons[buffid])
            abilitybar.BuffIdButtons[buffid]:Remove()
        end

    end)

    abilitybar:InvalidateLayout()
    abilitybar:ParentToHUD()
    abilitybar:MakePopup()
    abilitybar:SetMouseInputEnabled(false)
    abilitybar:SetKeyboardInputEnabled(false)

    hook.Add("ScoreboardShow", "MouseCapAbilityBar", function()
        abilitybar:SetMouseInputEnabled(true)
    end)
    hook.Add("ScoreboardHide", "MouseCapAbilityBar", function()
        abilitybar:SetMouseInputEnabled(false)
    end)
end

local mat = Material( "pp/blurscreen" )
hook.Add("HUDPaint", "DrawAbilityBar", function()
    if not IsValid(gmbp.AbilityBar) or gmbp.AbilityBarMFWC ~= LocalPlayer():GetFWClass() then CreateAbilityBar() end

    local abx, aby = gmbp.AbilityBar:GetPos()
    local abw, abh = gmbp.AbilityBar:GetSize()

    surface.SetDrawColor(0, 0, 0, 200)
    surface.DrawRect(abx, aby, abw, abh)

    surface.SetMaterial( mat )
    surface.SetDrawColor(255, 255, 255)
    mat:SetFloat( "$blur", 3.0 )
    mat:Recompute()
    if render then render.UpdateScreenEffectTexture() end

    render.SetScissorRect(abx, aby, abx+abw, aby+abh, true)

    surface.DrawTexturedRect( 0, 0, ScrW(), ScrH())

    render.SetScissorRect(0, 0, 0, 0, false)

    draw.RoundedBoxEx(6, abx+abw/2-70, aby-30, 140, 30, Color(0, 0, 0, 150), true, true)

    local total_length = GetGlobalFloat("roundend") - GetGlobalFloat("roundstart")
    local elapsed = CurTime() - GetGlobalFloat("roundstart")

    local frac = (elapsed/total_length)

    draw.RoundedBoxEx(6, abx+abw/2-70, aby-30, 140 * math.max(frac, 0.05), 30, Color(46, 204, 113, 50), true, frac > 0.95)

    local roundLeft = (GetGlobalFloat("roundend") - CurTime())  

    draw.SimpleText(
        (roundLeft < 0) and "Waiting" or gmbp.RoundStateNames[gmbp.GetRoundState()],
        "AbilityHotkey",
        abx+abw/2-65, aby-15,
        Color(255, 255, 255),
        TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER
    )

    draw.SimpleText(
        (roundLeft < 0) and "" or string.ToMinutesSeconds(math.Round(roundLeft)),
        "AbilityHotkey",
        abx+abw/2+65, aby-15,
        Color(255, 255, 255),
        TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER
    )

    -- TargetID

    --[[
local me = LocalPlayer()

    local players = player.GetAll()
    local local_eyepos = EyePos()
    local local_team = me:Team()
    local me_aimvec = me:GetAimVector()
    local me_aimvec_norm = me_aimvec:GetNormalized()

    for i=1,#players do
        local v = players[i]

        if not v:Alive() then continue end

        local targpos = v:EyePos()

        local vec_diff = (targpos - local_eyepos)
        local vec_diff_norm = vec_diff:GetNormalized()
        local dotprod = vec_diff_norm:Dot(me_aimvec_norm)

        if dotprod < 0.5 then continue end

        local screenpos = (targpos):ToScreen()
        if not screenpos.visible then continue end

        local unitdist = vec_diff:Length()
        if unitdist < 3000 and (dotprod > 0.98 or unitdist < 150) then
            local size = math.Clamp((3000 - unitdist)/3000, 0, 1)
            local alpha = (dotprod-0.98)*(1/0.02)

            local bw, bh = 120, 8

            surface.SetDrawColor(231, 76, 60, alpha*255)
            surface.DrawRect(screenpos.x-bw*0.5*size, screenpos.y, bw*size, bh*size)

            surface.SetDrawColor(46, 204, 113, alpha*255)
            surface.DrawRect(screenpos.x-(bw/2-1)*size, screenpos.y+1, (bw-2)*size, (bh-2)*size)
        end
    end
    ]]

end)