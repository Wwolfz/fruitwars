concommand.Add("fw_selclass", function(ply, cmd, args)
	if args[1] == "melon" then ply:SetFWClass(CLASS_MELON)
	elseif args[1] == "orange" then ply:SetFWClass(CLASS_ORANGE)
	elseif args[1] == "banana" then ply:SetFWClass(CLASS_BANANA) end

	if ply:Alive() then ply:KillSilent() end
end)

concommand.Add("fw_selteam", function(ply, cmd, args)
	local teamnum = tonumber(args[1])
	if teamnum == TEAM_ONE or teamnum == TEAM_TWO then
		ply:SetTeam(teamnum)
		if ply:Alive() then ply:KillSilent() end
	end
end)

concommand.Add("fw_selbotclasses", function(ply, cmd, args)
	for _,p in pairs(player.GetAll()) do
		if p:IsBot() then
			player_manager.SetPlayerClass( p, "player_melon" )
			p:SetFWClass(CLASS_MELON)
			p:Spawn()
		end
	end
end)