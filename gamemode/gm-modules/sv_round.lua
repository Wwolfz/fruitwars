
local default_round_lengths = {
	[ROUND_PRE] = 5,
	[ROUND_ACTIVE] = 60 * 7,
	[ROUND_POST] = 5
}

local round_actions = {
	[ROUND_PRE] = function()
		game.CleanUpMap()
		for _,ply in pairs(player.GetAll()) do
			ply:Spawn()
		end
	end,
	[ROUND_ACTIVE] = function()
		for _,ply in pairs(player.GetAll()) do
			if not ply:Alive() then
				ply:Spawn()
			end
		end
	end
}

function gmbp.SetRoundState(state)
	MsgN("Round state changing to ", state)

	SetGlobalFloat("roundstart", CurTime())
	SetGlobalFloat("roundend", CurTime() + default_round_lengths[state])

	local ract = round_actions[state]
	if ract then
		local stat, err = pcall(ract)
		if not stat then
			MsgN("Roundstate action failed: ", err)
		end
	end

	return SetGlobalInt("roundstate", state)
end

-- A quick two liner to make sure the RoundTick timer doesnt miss simultaneous deaths
local latest_death_team
hook.Add("PlayerDeath", "PlyDeathTeam", function(ply) latest_death_team = ply:Team() end)

function gmbp.RoundTick()
	local state = gmbp.GetRoundState()

	local round_expired = GetGlobalFloat("roundend") < CurTime()
	if round_expired then

		if state == ROUND_PRE then
			local teamplys = {team.GetPlayers(TEAM_ONE), team.GetPlayers(TEAM_TWO)}

			if #teamplys[1] > 0 and #teamplys[2] > 0 then
				gmbp.SetRoundState(ROUND_ACTIVE)
			end
		elseif state == ROUND_ACTIVE then
			gmbp.SetRoundState(ROUND_POST)
		elseif state == ROUND_POST then
			gmbp.SetRoundState(ROUND_PRE)
		end

	elseif state == ROUND_ACTIVE then

		local teamplys = {team.GetPlayers(TEAM_ONE), team.GetPlayers(TEAM_TWO)}

		local aliveplys = {{}, {}}
		table.foreach(teamplys[1], function(_, p) if not p:Alive() then return end table.insert(aliveplys[1], p) end)
		table.foreach(teamplys[2], function(_, p) if not p:Alive() then return end table.insert(aliveplys[2], p) end)

		local winnerteam

		if #aliveplys[1] > 0 and #aliveplys[2] == 0 then
			winnerteam = TEAM_ONE
		elseif #aliveplys[1] == 0 and #aliveplys[2] > 0 then
			winnerteam = TEAM_TWO
		elseif #aliveplys[1] == 0 and #aliveplys[2] == 0 then
			winnerteam = latest_death_team or -1
		end

		if winnerteam then
			local winner_string = winnerteam == -1 and "neither" or team.GetName(winnerteam)

			-- TODO more sophisticated way to do this
			BroadcastLua([[chat.AddText(Color(255, 127, 0), "]] .. winner_string .. [[ wins the round!")]])

			gmbp.SetRoundState(ROUND_POST)
		end

	end
end

timer.Create("roundticker", 0.5, 0, gmbp.RoundTick)