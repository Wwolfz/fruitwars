-- Random notes on buffs:
-- Buffs can be prematurely stopped using buff:Stop()

gmbp.Buffs = {
	["movespeed"] = {
		default_length = 10,

		is_positive = function(data) return data.mul >= 1 end,

		icon = "icon16/user_go.png",
		description = function(data) return string.format("%s movement speed by %d%%", data.mul >= 1 and "Increases" or "Decreases", (data.mul-1)*100) end,

		start = function(self, ply, datatbl)
			gmbp.AddCumulativeHook("MovementSpeed", "MoveSpeedBuff" .. ply:EntIndex() .. self.start, function(tply)
				if ply == tply then return datatbl.mul end
			end)
		end,
		stop = function(self, ply, datatbl)
			gmbp.RemoveCumulativeHook("MovementSpeed", "MoveSpeedBuff" .. ply:EntIndex() .. self.start)
		end 
	},
	["attackspeed"] = {
		default_length = 10,

		is_positive = function(data) return data.mul <= 1 end,

		icon = "icon16/bomb.png",
		description = function(data) return string.format("%s attack speed by %d%%", data.mul <= 1 and "Increases" or "Decreases", (1-data.mul)*100) end,

		start = function(self, ply, datatbl)
			gmbp.AddCumulativeHook("AttackSpeed", "AttackSpeedBuff" .. ply:EntIndex() .. self.start, function(tply)
				if ply == tply then return datatbl.mul end
			end)
		end,
		stop = function(self, ply, datatbl)
			gmbp.RemoveCumulativeHook("AttackSpeed", "AttackSpeedBuff" .. ply:EntIndex() .. self.start)
		end 
	},
	["homingprojectiles"] = {
		default_length = 20,

		is_positive = function() return true end,

		icon = "icon16/vector.png",
		description = function() return "Makes your attacks home in on nearby enemy targets" end,

		start = function(self, ply, datatbl)
			local projectiles = 0
			hook.Add("FWModifyProjectile", "HomingProjBuff" .. ply:EntIndex() .. self.start, function(wep, proj)
				if wep.Owner == ply then
					proj.HomingPreset = {type="nearenemy", strength=0.1}
					proj:SetTrailColor(Vector(155/255, 89/255, 182/255))

					projectiles = projectiles + 1
					if projectiles >= (datatbl.projectiles or 1) then
						self:Stop()
					end
				end
			end)
		end,
		stop = function(self, ply, datatbl)
			hook.Remove("FWModifyProjectile", "HomingProjBuff" .. ply:EntIndex() .. self.start)
		end 
	},
	["hpmodprojectile"] = {
		default_length = 20,

		is_positive = function() return true end,

		icon = "icon16/bug_edit.png",
		description = function() return "Makes your attack poison the target" end,

		start = function(self, ply, datatbl)
			local projectiles = 0
			hook.Add("FWModifyProjectile", "HPModBuff" .. ply:EntIndex() .. self.start, function(wep, proj)
				if wep.Owner == ply then
					proj.AfterTargDamaged = function(self, tply)
						tply:GiveFWBuff("healthmod", {length=datatbl.modlength, off=datatbl.off, att=ply, infl=ply:GetActiveWeapon(), dmgtype=datatbl.dmgtype})
					end
					proj:SetTrailColor(Vector(102/255, 204/255, 26/255))

					projectiles = projectiles + 1
					if projectiles >= (datatbl.projectiles or 1) then
						self:Stop()
					end
				end
			end)
		end,
		stop = function(self, ply, datatbl)
			hook.Remove("FWModifyProjectile", "HPModBuff" .. ply:EntIndex() .. self.start)
		end 
	},
	["stunprojectile"] = {
		default_length = 20,

		is_positive = function() return true end,

		icon = "icon16/building_edit.png",
		description = function() return "Stuns the target" end,

		start = function(self, ply, datatbl)
			local projectiles = 0
			hook.Add("FWModifyProjectile", "StunModBuff" .. ply:EntIndex() .. self.start, function(wep, proj)
				if wep.Owner == ply then
					proj.AfterTargDamaged = function(self, ply)
						ply:GiveFWBuff("stun", {length=datatbl.modlength})
					end
					proj:SetTrailColor(Vector(52/255, 73/255, 94/255))

					projectiles = projectiles + 1
					if projectiles >= (datatbl.projectiles or 1) then
						self:Stop()
					end
				end
			end)
		end,
		stop = function(self, ply, datatbl)
			hook.Remove("FWModifyProjectile", "StunModBuff" .. ply:EntIndex() .. self.start)
		end 
	},
	["dmgforce"] = {
		default_length = 20,

		is_positive = function() return true end,

		icon = "icon16/wand.png",
		description = function() return "Throws hit target to air" end,

		start = function(self, ply, datatbl)
			local projectiles = 0
			hook.Add("FWModifyProjectile", "DmgForceBuff" .. ply:EntIndex() .. self.start, function(wep, proj)
				if wep.Owner == ply then
					proj.AfterTargDamaged = function(self, targ, dmginfo)
						targ:SetVelocity(Vector(0, 0, 1000))
						--dmginfo:SetDamageForce(datatbl.impulse*1000)
					end
					proj:SetTrailColor(Vector(26/255, 188/255, 156/255))

					projectiles = projectiles + 1
					if projectiles >= (datatbl.projectiles or 1) then
						self:Stop()
					end
				end
			end)
		end,
		stop = function(self, ply, datatbl)
			hook.Remove("FWModifyProjectile", "DmgForceBuff" .. ply:EntIndex() .. self.start)
		end 
	},
	-- This is so called "instant buff", which is triggered only once and then removes itself
	["resetfiredelay"] = {
		default_length = 0.1,

		is_positive = function() return true end,

		icon = "icon16/control_end_blue.png",
		description = function() return "Resets basic attack delay" end,

		tick = function(self, ply, datatbl)
			ply:GetActiveWeapon():SetNextPrimaryFire(CurTime())
			return true
		end
	},
	["abilitystandby"] = { -- This does nothing serverside. Acts as notifier that some ability is waiting to be used
		default_length = 10,

		is_positive = function() return true end,

		icon = "icon16/clock.png",
		description = function() return "Ability ready! Use basic attack to fire the ability." end
	},
	["healthmod"] = {
		default_length = 10,

		is_positive = function(data) return data.off >= 0 end,

		icon = "icon16/bug.png",
		description = function(data) return string.format("%s (%s health per second)", data.off>=0 and "Cure" or "Poison", data.off >= 0 and "+"..data.off or data.off) end,

		tick = function(self, ply, datatbl)
			if ply:Alive() and gmbp.TickEvery(self, "HealthMod", 1) then
				if datatbl.off >= 0 then
					ply:AddHealth(datatbl.off)
				else
					local dmginfo = DamageInfo()
					dmginfo:SetAttacker(IsValid(datatbl.att) and datatbl.att or game.GetWorld())

					if IsValid(datatbl.infl) then dmginfo:SetInflictor(datatbl.infl) end
					dmginfo:SetDamageType(datatbl.dmgtype or DMG_GENERIC)
					dmginfo:SetDamage(-datatbl.off)
					ply:TakeDamageInfo(dmginfo)
				end
			end
		end
	},
	["stun"] = {
		default_length = 10,

		is_positive = function(data) return false end,

		icon = "icon16/building.png",
		description = function(data) return "Stun!" end,

		start = function(self, ply, datatbl)
			ply:Freeze(true)
		end,
		stop = function(self, ply, datatbl)
			ply:Freeze(false)
		end,
	},
	["resizeprojectile"] = {
		default_length = 10,

		is_positive = function(data) return data.mul >= 1 end,

		icon = "icon16/shape_handles.png",
		description = function(data) return string.format("Projectile's damage and size will be %d%% of normal. Speed is changed inversely.", (data.mul*100)) end,

		start = function(self, ply, datatbl)
			local projectiles = 0
			hook.Add("FWModifyProjectile", "ResizeProjBuff" .. ply:EntIndex() .. self.start, function(wep, proj)
				if wep.Owner == ply then
					proj:SetFWModelScale(datatbl.mul)
					proj.PVPDamage = proj.PVPDamage * (datatbl.dmgmul or datatbl.mul)
					proj.VelocityVector = proj.VelocityVector * (1/datatbl.mul)

					projectiles = projectiles + 1
					if projectiles >= (datatbl.projectiles or 1) then
						self:Stop()
					end
				end
			end)
		end,
		stop = function(self, ply, datatbl)
			hook.Remove("FWModifyProjectile", "ResizeProjBuff" .. ply:EntIndex() .. self.start)
		end 
	},

}

local plymeta = FindMetaTable("Player")

if SERVER then
	util.AddNetworkString("fw_plybuffed")
	util.AddNetworkString("fw_plybuffaborted")

	local buff_meta = {
		Stop = function(self)
			self.ply:StopFWBuff(self.id)

			net.Start("fw_plybuffaborted")
			net.WriteEntity(self.ply)
			net.WriteString(self.id)
			net.Send(self.ply)

			if self.FStopCallbacks then
				for _,fn in pairs(self.FStopCallbacks) do
					fn(self)
				end
			end

		end,
		OnForcedStop = function(self, fn)
			self.FStopCallbacks = self.FStopCallbacks or {}
			table.insert(self.FStopCallbacks, fn)
		end
	}
	buff_meta.__index = buff_meta

	function plymeta:GiveFWBuff(name, datatbl)
		local buffmt = gmbp.Buffs[name]
		if not buffmt then return ErrorNoHalt("Trying to give invalid buff '" .. name .. "'") end

		datatbl = datatbl or {}

		-- length 0 = is always active until Stop() is called, see notes
		local length = datatbl.length or buffmt.default_length

		self.FW_Buffs = self.FW_Buffs or {}

		local buff = {
			name=name,
			id=name .. CurTime(),
			ply=self,
			start=CurTime(),
			buffmt=buffmt, -- Cached here for faster access from tick etc
			length=length,
			data=datatbl
		}
		setmetatable(buff, buff_meta)

		table.insert(self.FW_Buffs, buff)

		net.Start("fw_plybuffed")
		net.WriteEntity(self)
		net.WriteString(name)
		net.WriteString(buff.id)
		net.WriteFloat(length)
		net.WriteTable(datatbl)
		net.Send(self)

		if buffmt.start then
			buffmt.start(buff, self, datatbl)
		end

		return buff
	end

	-- Used internally
	function plymeta:ReduceFWBuffs(name, def, fn)
		if not self.FW_Buffs then return def end

		for k,buff in pairs(self.FW_Buffs) do
			if not name or buff.name == name then
				def = fn(def, buff)
			end
		end

		return def
	end

	function plymeta:HasFWBuffName(name)
		if not self.FW_Buffs then return end
		for k,buff in pairs(self.FW_Buffs) do
			if buff.name == name then
				return true
			end
		end
		return false
	end

	function plymeta:HasFWBuff(id)
		if not self.FW_Buffs then return end
		for k,buff in pairs(self.FW_Buffs) do
			if buff.id == id then
				return true
			end
		end
		return false
	end

	function plymeta:StopFWBuff(id)
		if not self.FW_Buffs then return end
		for k,buff in pairs(self.FW_Buffs) do
			if buff.id == id then
				if buff.buffmt.stop then buff.buffmt.stop(buff, self, buff.data) end
				table.remove(self.FW_Buffs, k)
			end
		end
	end

	function plymeta:StopFWBuffName(name)
		if not self.FW_Buffs then return end
		for k,buff in pairs(self.FW_Buffs) do
			if buff.name == name then
				if buff.buffmt.stop then buff.buffmt.stop(buff, self, buff.data) end
				table.remove(self.FW_Buffs, k)
			end
		end
	end

	function plymeta:StopFWBuffs()
		if not self.FW_Buffs then return end
		for k,buff in pairs(self.FW_Buffs) do
			if buff.buffmt.stop then buff.buffmt.stop(buff, self, buff.data) end
			table.remove(self.FW_Buffs, k)
		end
	end

	hook.Add("Tick", "PlayerBuffTick", function()
		for _,ply in pairs(player.GetAll()) do
			if ply.FW_Buffs then
				for _,buff in pairs(ply.FW_Buffs) do
					-- Initialize should_remove with "expired" boolean
					local should_remove = (buff.start+buff.length) <= CurTime() 
					
					-- If not killed by expiration, check tick and tick's return value (if true=remove buff)
					if not should_remove and buff.buffmt.tick and buff.buffmt.tick(buff, ply, buff.data) == true then
						should_remove = true
					end

					if should_remove then
						if buff.buffmt.stop then buff.buffmt.stop(buff, ply, buff.data) end
						table.RemoveByValue(ply.FW_Buffs, buff)
					end
				end
			end
		end
	end)
end

if CLIENT then

end