local sin,cos,rad = math.sin,math.cos,math.rad; --Only needed when you constantly calculate a new polygon, it slightly increases the speed.
local function GenerateFancyPoly(x,y,radius,quality)
    local circle = {};
    local tmp = 0;
    for i=1,quality do
    	local radius = radius + math.sin(CurTime()+(i*0.8))*10
        tmp = rad(i*360)/quality
        circle[i] = {x = x + cos(tmp)*radius,y = y + sin(tmp)*radius};
    end
    return circle;
end

local aoe_sel_mat = Material("gui/progress_cog.png", "smooth")
local starg_sel_mat = Material("gui/workshop_rocket.png", "smooth")

hook.Add("HUDPaint", "DrawAbilitySelector", function()
	if not gmbp.SelectorType then return end

	if gmbp.SelectorType.type == "singletarget" then
		surface.SetDrawColor(255, 255, 255)
		surface.DrawOutlinedRect(ScrW()/2-24, ScrH()/2-24, 48, 48)
	end
end)

local fancy_polys = {
	[20] = GenerateFancyPoly(0, 0, 20, 32),
	[50] = GenerateFancyPoly(0, 0, 50, 32),
	[100] = GenerateFancyPoly(0, 0, 100, 32),
	[150] = GenerateFancyPoly(0, 0, 150, 32),
}

local matOutline = Material("models/props_combine/tprings_globe")
hook.Add("PostDrawTranslucentRenderables", "DrawAbilitySelector", function()

	if not gmbp.SelectorType then return end

	if gmbp.SelectorType.type == "aoe" then

		local tr = LocalPlayer():GetEyeTrace()
		if tr.HitNormal.z < (tr.HitNormal.x+tr.HitNormal.y) then return end

		cam.IgnoreZ(true)

		cam.Start3D2D(tr.HitPos + tr.HitNormal, Angle(0, (CurTime()*100)%360, 0), 1)
		draw.NoTexture()

		render.ClearStencil()
		render.SetStencilEnable( true )

		local function HollowCircle(inner_radius, outer_radius)
			render.SetStencilFailOperation( STENCILOPERATION_KEEP )
			render.SetStencilZFailOperation( STENCILOPERATION_REPLACE )
			render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
			render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )
			render.SetStencilReferenceValue( 1 )

			surface.SetDrawColor(0, 0, 0, 1)
			surface.DrawPoly(fancy_polys[outer_radius] or GenerateFancyPoly(0, 0, outer_radius, 32))

			render.SetStencilReferenceValue( 2 )

			surface.SetDrawColor(0, 0, 0, 1)
			surface.DrawPoly(fancy_polys[inner_radius] or GenerateFancyPoly(0, 0, inner_radius, 32))
		end
	 
		HollowCircle(100, 150)
		HollowCircle(20, 50)

		render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
		render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
		render.SetStencilReferenceValue( 1 )
	 
		render.SetMaterial( matOutline )
		render.DrawScreenQuad()
	 
		render.SetStencilEnable( false )

		cam.End3D2D()
		cam.IgnoreZ(false)

		LocalPlayer():DrawModel() -- Draw lplayer so we're not behind selector
	end
end)