local tp_distance = CreateClientConVar( "ttt_thirdperson_dist", 125, true, false )
local tp_side = CreateClientConVar( "ttt_thirdperson_side", 0, true, false )

hook.Add("CalcView", "ThirdPersonCamera", function(ply, origin, angles, fov, znear, zfar)
    if IsValid(ply:GetVehicle()) then return end
    if IsValid(ply:GetDrivingEntity()) then return end

    local view = {}
    view.origin = origin
    view.angles = angles
    view.fov = fov
    view.znear  = znear
    view.zfar   = zfar
    view.drawviewer = true
 
    local radius = math.Clamp(tp_distance:GetFloat(), 0, 200)

    local activewep = ply:GetActiveWeapon()

    if IsValid(activewep) and activewep.GetIronsights then
        local bIron = activewep:GetIronsights()
        local mul = bIron and tp_ironmul:GetFloat() or 1
        activewep.WW_GoForMul = math.Approach(activewep.WW_GoForMul or 1, mul, tp_ironzoom:GetFloat())
        radius = radius * activewep.WW_GoForMul
    end

    -- Trace back from the original eye position, so we don't clip through walls/objects
    local TargetOrigin = view.origin + ( view.angles:Forward() * -radius ) + view.angles:Up() * 10

    if tp_side:GetInt() == 0 then TargetOrigin = TargetOrigin - view.angles:Right()*30
    elseif tp_side:GetInt() == 1 then TargetOrigin = TargetOrigin + view.angles:Right()*30 end
    local WallOffset = 4

    local tr = util.TraceHull({
        start   = view.origin,
        endpos  = TargetOrigin,
        filter  = function(ent) return not (ent == ply or ent:GetOwner() == ply or ent:GetClass() == "projectile_base") end,
        mins    = Vector( -WallOffset, -WallOffset, -WallOffset ),
        maxs    = Vector( WallOffset, WallOffset, WallOffset ),
    })

    view.origin = tr.HitPos
    view.drawviewer = true

    --
    -- If the trace hit something, put the camera there.
    --
    if ( tr.Hit && !tr.StartSolid) then
        view.origin = view.origin + tr.HitNormal * WallOffset
    end

    return view
end)

surface.CreateFont("FWTargetID", {
    font = "Roboto",
    size = 26
})

hook.Add("HUDPaint", "DrawCrosshair", function()
    local ply = LocalPlayer()
    local tr = util.TraceHull({
        start   = ply:GetShootPos(),
        endpos  = ply:GetShootPos() + ply:GetAimVector()*9999, 
        filter  = function(ent) return ent ~= ply and ent:GetOwner() ~= ply end
    })

    --MsgN(tr.Entity, " ", tr.Entity:GetOwner())

    local hps = tr.HitPos:ToScreen()
    local cx, cy, crad = hps.x-5, hps.y-5, 7
    surface.DrawCircle(cx, cy, crad, team.GetColor(LocalPlayer():Team()))

    local rad = CurTime()
    local rad_opp = rad+math.pi

    surface.DrawLine(cx+math.cos(rad)*crad, cy+math.sin(rad)*crad, cx+math.cos(rad_opp)*crad, cy+math.sin(rad_opp)*crad)    

    if tr.Entity:IsPlayer() then
        local nick = tr.Entity:Nick()
        surface.SetFont("FWTargetID")
        local tw, th = surface.GetTextSize(nick)

        surface.SetTextColor(255, 0, 0)
        surface.SetTextPos(hps.x + 50, hps.y-th/2)
        surface.DrawText(nick)

        render.SetScissorRect(0, 0, hps.x + 50 + tw*(tr.Entity:Health()/100), ScrH(), true)

        surface.SetTextColor(0, 255, 0)
        surface.SetTextPos(hps.x + 50, hps.y-th/2)
        surface.DrawText(nick)

        render.SetScissorRect(0, 0, 0, 0, false)
    end
end)

hook.Add("ShouldDrawLocalPlayer", "ThirdPersonDrawSelf", function(ply)
	return true
end)
