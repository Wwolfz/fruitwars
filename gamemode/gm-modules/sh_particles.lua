if SERVER then
	util.AddNetworkString("fw_particle")
	function gmbp.SpawnParticle(pos, datatbl)
		net.Start("fw_particle")
		net.WriteVector(pos)
		net.WriteTable(datatbl or {})
		net.Broadcast()
	end
end
if CLIENT then
	function gmbp.SpawnParticle(pos, datatbl)
		datatbl = datatbl or {}

		local sprite = datatbl.Sprite or "sprites/light_glow02_add"
		local velocity = datatbl.Velocity or VectorRand() * 3
		local dietime = datatbl.DieTime or 4
		local startalpha, endalpha = datatbl.StartAlpha or 255, datatbl.EndAlpha or 255
		local startsize, endsize = datatbl.StartSize or 40, datatbl.EndSize or 40
		local roll = datatbl.Roll or 0
		local clr = datatbl.Color or Color(255, 255, 255)
		local dynlight = datatbl.DynLight

		if not gmbp.ParticleEmitter then
			gmbp.ParticleEmitter = ParticleEmitter( Vector(0, 0, 0), false ) 
		end

		local particle = gmbp.ParticleEmitter:Add( sprite, pos ) 
		particle:SetVelocity( velocity ) 
		particle:SetDieTime( dietime ) 
		particle:SetStartAlpha( startalpha ) 
		particle:SetEndAlpha( endalpha ) 
		particle:SetStartSize( startsize ) 
		particle:SetEndSize( endsize ) 
		particle:SetRoll( roll ) 
		particle:SetColor( clr.r, clr.g, clr.b )
		if datatbl.Gravity then particle:SetGravity(datatbl.Gravity) end

		if dynlight then
			local dlight = DynamicLight(0)

			dlight.Pos = pos
			dlight.r = clr.r
			dlight.g = clr.g
			dlight.b = clr.b
			dlight.Brightness = 1
			dlight.Size = startsize
			dlight.Decay = startsize*2
			dlight.DieTime = CurTime() + dietime
            dlight.Style = 0
		end

		return particle
	end
	net.Receive("fw_particle", function()
		gmbp.SpawnParticle(net.ReadVector(), net.ReadTable())
	end)
end