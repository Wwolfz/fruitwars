hook.Add("PlayerBindPress", "OverridePlayerBinds", function(ply, bind, press)
	local fwcls = ply:GetFWClass()

	-- Abilities
	local slotidx = tonumber(string.match(bind, "slot(%d)"))
	if slotidx and gmbp.Abilities[fwcls] and gmbp.Abilities[fwcls][slotidx] then
		local abilityslot = gmbp.Abilities[fwcls][slotidx]
		if abilityslot.select_type == "aoe" or abilityslot.select_type == "singletarg" then
			gmbp.SelectorType = {
				abilityid = slotidx,
				type = abilityslot.select_type
			}
		else
			net.Start("fw_ability")
			net.WriteUInt(slotidx, 8)
			net.SendToServer()
		end
		return true
	end

	if bind == "+attack" and press and gmbp.SelectorType then
		local tr = LocalPlayer():GetEyeTrace()
		--MsgN(tr.HitNormal)
		--if tr.HitNormal.z < (tr.HitNormal.x+tr.HitNormal.y) then
			net.Start("fw_ability")
			net.WriteUInt(gmbp.SelectorType.abilityid, 8)
			net.WriteVector(tr.HitPos)
			net.SendToServer()
		--end
		gmbp.SelectorType = nil
		return true
	end
end)