
surface.CreateFont("FWScoreboardNick", {
	font = "Roboto",
	size = 30
})

function GM:ScoreboardShow()
	if IsValid(gmbp.Scoreboard) then gmbp.Scoreboard:Remove() end

	gmbp.Scoreboard = vgui.Create("DPanel")
	gmbp.Scoreboard:SetPos(ScrW()/2-400, 200)
	gmbp.Scoreboard:SetSize(800, 500)

	do -- Class picker

		local classlist = vgui.Create("DPanelList", gmbp.Scoreboard)
		classlist:SetPos(375, 54)
		classlist:SetSize(50, 300)
		classlist:SetSpacing(2)

		for _,cls in ipairs{"melon","orange","banana"} do
			local clsbtn = vgui.Create("DButton", classlist)
			clsbtn:SetText("")
			clsbtn:SetSize(50, 50)

			clsbtn.DoClick = function()
				RunConsoleCommand("fw_selclass", cls)
			end

			local clsmodel = vgui.Create("ModelImage", clsbtn)
			clsmodel:Dock(FILL)

			local model = weapons.Get("weapon_basic_" .. cls).WorldModel
			clsmodel:SetModel(model)
			clsmodel:SetMouseInputEnabled(false)

			clsbtn.Paint = function(self, w, h)
				surface.SetDrawColor(0, 0, 0)
				surface.DrawRect(0, 0, w, h)

				surface.SetDrawColor(HSVToColor(220, 0.6, self.Hovered and 0.8 or 0.75))
				surface.DrawRect(2, 2, w-4, h-4)

				--surface.SetMaterial(mat_wand)
				--surface.SetDrawColor(255, 255, 255)
				--surface.DrawTexturedRect(9, 9, 32, 32)
			end

			classlist:AddItem(clsbtn)
		end

		--classlist:Hide()

		local labelpanel = vgui.Create("DButton", gmbp.Scoreboard)
		labelpanel:SetText("")
		labelpanel.DoClick = function()
			if classlist:IsVisible() then classlist:Hide() else classlist:Show() end
		end

		local mat_wand = Material("icon32/wand.png")

		labelpanel.Paint = function(self, w, h)
			surface.SetDrawColor(0, 0, 0)
			surface.DrawRect(0, 0, w, h)

			surface.SetDrawColor(HSVToColor(60, 0.6, self.Hovered and 0.8 or 0.75))
			surface.DrawRect(2, 2, w-4, h-4)

			surface.SetMaterial(mat_wand)
			surface.SetDrawColor(255, 255, 255)
			surface.DrawTexturedRect(9, 9, 32, 32)
		end
		labelpanel:SetPos(375, 2)
		labelpanel:SetSize(50, 50)
	end

	for i=0,1 do

		local teamid = i+1

		local teamlist = vgui.Create("DPanelList", gmbp.Scoreboard)
		teamlist:SetPos(i*425, 0)
		teamlist:SetSize(375, 500)
		teamlist:SetPadding(2)
		teamlist:SetSpacing(2)

		do
			local labelpanel = vgui.Create("DButton", teamlist)
			labelpanel:SetText("")
			labelpanel.DoClick = function()
				RunConsoleCommand("fw_selteam", teamid)
			end
			labelpanel.Paint = function(self, w, h)
				local textcolor = hook.Call("FWDrawSBTeamLabel", GAMEMODE, teamid, w, h)

				if not textcolor then
					surface.SetDrawColor(0, 0, 0)
					surface.DrawRect(0, 0, w, h)

					local hue, s, v = ColorToHSV(team.GetColor(teamid))

					surface.SetDrawColor(HSVToColor(hue, (self.Hovered or LocalPlayer():Team() == teamid) and s+0.2 or s, v))
					surface.DrawRect(2, 2, w-4, h-4)
				end

				draw.SimpleText(team.GetName(teamid), "FWScoreboardNick", w/2, 10, Color(0, 0, 0), TEXT_ALIGN_CENTER)
			end
			labelpanel:SetTall(50)
			labelpanel:DockMargin(0, 0, 2, 2)
			teamlist:AddItem(labelpanel)
		end

		for _,ply in pairs(player.GetAll()) do
			if ply:Team() ~= teamid then continue end

			local plypanel = vgui.Create("DButton", teamlist)
			plypanel:SetText("")
			plypanel.Paint = function(self, w, h)
				local hue, s, v = ColorToHSV(team.GetColor(teamid))
				surface.SetDrawColor(HSVToColor(hue, s-0.2, v))
				surface.DrawRect(0, 0, w, h)
				draw.SimpleText(ply:Nick(), "FWScoreboardNick", 10, h/2, Color(0, 0, 0), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			end
			plypanel:SetTall(40)
			plypanel:DockMargin(0, 0, 2, 2)

			plypanel.DoRightClick = function()
				if not IsValid(ply) then return end

				local menu = DermaMenu()
				menu:AddOption(ply:Nick(), function() ply:ShowProfile() end):SetImage("icon16/user.png")
				menu:AddOption(string.format("Ping: %d", ply:Ping()), function() end)
				menu:AddSpacer()
				menu:AddOption(ply:IsMuted() and "Unmute" or "Mute", function() ply:SetMuted(not ply:IsMuted()) end)
				menu:AddOption("Copy SteamID", function() SetClipboardText(ply:SteamID()) end)
				menu:Open()
			end

			teamlist:AddItem(plypanel)
		end
	end

	gui.EnableScreenClicker(true)
end
function GM:ScoreboardHide()
	if IsValid(gmbp.Scoreboard) then gmbp.Scoreboard:Remove() end
	gui.EnableScreenClicker(false)
end