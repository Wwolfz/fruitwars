AddCSLuaFile()
DEFINE_BASECLASS( "player_default" )

local PLAYER = {}

PLAYER.DisplayName	= "Melon Class"

PLAYER.WalkSpeed = 280
PLAYER.RunSpeed	= 280
PLAYER.CrouchedWalkSpeed = 0.2
PLAYER.DuckSpeed	= 0.3
PLAYER.UnDuckSpeed	= 0.3
PLAYER.JumpPower	= 120
PLAYER.CanUseFlashlight = true
PLAYER.MaxHealth	= 100
PLAYER.StartHealth	= 100
PLAYER.StartArmor	= 50
PLAYER.DropWeaponOnDie	= false
PLAYER.TeammateNoCollide = true
PLAYER.AvoidPlayers	= false

function PLAYER:SetupDataTables()
	BaseClass.SetupDataTables( self )
	self.Player:NetworkVar("Vector", 0, "FWCooldowns")
end

function PLAYER:Spawn()
	BaseClass.Spawn( self )
end

function PLAYER:Loadout()
	self.Player:Give("weapon_basic_melon")
end

player_manager.RegisterClass( "player_melon", PLAYER, "player_default" )